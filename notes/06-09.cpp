// -----------
// Wed,  9 Jun
// -----------

/*
Website
	https://www.cs.utexas.edu/users/downing/cs371g/

Cold Calling
	you're only called ONCE per rotation
	it's totally fine to not know the answer, the idea is to discuss and to learn

Lecture
	1-3 pm

Office Hours
	Glenn
		Austin
		TTh 3-4 pm

	Amogh ('22)
		Austin
		MW 3-4 pm

Canvas
	personal questions
	check your grades regularly!

Discord
	chat
	please be generous and kind

GitLab
	notes
	examples
	exercises

Piazza
	class questions
	ask and answer questions
	please be proactive

Zoom
	must use UT EID credentials (@eid.utexas.edu)
	classes recorded
	published later that day
	5.6.6
*/

/*
Breakout Rooms
	7 breakout rooms

	Full Name: Room #5: <name1>, <name2>, ...
	EID:       <eid1>, <eid2>, ...

	EVERYONE turn on video
	leave the shared screen up
	stay in the breakout room
	when done ask us to visit
*/

/*
takeaways
	assertions are good for preconditions, postconditions, and loop invariants
	they're also good as computational comments
	they're not good for testing
	they're not good for user errors
*/

/*
reminders
	Zoom 5.6.7
    announcements
	virtual tokens
	Academic Integrity Quiz
	quiz results
*/

n + (n >> 1) + 1

(3n + 1) / 2
3n/2 + 1/2
n + n/2 + 1/2
// this is really the same as
n + n/2 + 1
n + (n >> 1) + 1

int cycle_length (int n) {
    assert(n > 0);
    int c = 1;
    while (n > 1) {
        if ((n % 2) == 0)
            n = (n / 2);
        else
            n = (3 * n) + 1;
        ++c;}
    assert(c > 0);
    return c;}

mcl(10, 100)
b = 10
e = 100
m = (e / 2) + 1 = 51
if b < m, yes
	mcl(10, 100) = mcl(m, e) = mcl(51, 100)

take any number between 10 and 50
some numbering of doublings will land you in the range 51 to 100

mcl(200, 300)
b = 200
e = 300
m = 151
if b < m, no

/*
lazy caching
array
the index would be the number that we're computing the cycle length of
the value would be the cycle length
let's build a one-million element array

1
2
3 8
4
5 6
6
7
8
9
10 7
*/

/*
imagine this input
1 10
we can store more than 10 values in the cache
*/

/*
eager caching
let's build the cache before reading anything!!!
*/

/*
meta caching
let's write a program that will NOT go to HackerRank
compute all million cycle lengths
write a second program that WILL go to HackerRank
and this program will contain
*/

int a[] = {...one million numbers...}
// HackerRank limitation on source file size is 50k

/*
the input is ranges of numbers

instead of caching cycle lengths
we cached max cycle lengths for ranges
*/

/*
let's cache
1    1000
1001 2000
2001 3000
3001 4000
...
990001 1,000,000

1000 numbers
*/

int a[] = {...one thousand numbers...}

/*
mcl(1500, 3500)
compute 1500 to 2000
look up 2001 to 3000
compute 3001 to 3500
*/

/*
1800 to 1901
my cache doesn't help
*/

// acceptance test

// input

1 10
100 200
201 210
900 1000

// output

1 10 20
100 200 125
201 210 89
900 1000 174
