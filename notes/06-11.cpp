// -----------
// Fri, 11 Jun
// -----------

/*
Website
	https://www.cs.utexas.edu/users/downing/cs371g/

Cold Calling
	you're only called ONCE per rotation
	it's totally fine to not know the answer, the idea is to discuss and to learn

Lecture
	1-3 pm

Office Hours
	Glenn
		Austin
		TTh 3-4 pm

	Amogh ('22)
		Austin
		MW 3-4 pm

Canvas
	personal questions
	check your grades regularly!

Discord
	chat
	please be generous and kind

GitLab
	notes
	examples
	exercises

Piazza
	class questions
	ask and answer questions
	please be proactive

Zoom
	must use UT EID credentials (@eid.utexas.edu)
	classes recorded
	published later that day
	5.6.6
*/

/*
Breakout Rooms
	Full Name: Room #5: <name1>, <name2>, ...
	EID:       <eid1>, <eid2>, ...

	EVERYONE turn on video
	leave the shared screen up
	stay in the breakout room
	when done ask us to visit
	after the visit from me or Amogh, then submit
*/

/*
takeaways
	assertions are good for preconditions, postconditions, and loop invariants
	they're also good as computational comments
	they're not good for testing
	they're not good for user errors
	for standalone increment prefer preincrement
*/

/*
reminders
	Zoom 5.6.7
    announcements
	Academic Integrity Quiz
	Collatz Poll
*/

int i = 2;
i = 3;     // that makes i an l-value (l-values are addressable)
int j = 3;
j = i;     // l-values are always also r-values

i = 2;     // that makes 2 an r-value (r-values are not addessable)
2 = i;     // r-values are not l-values

int i = 2;
int j = 3;
int k = (i + j); // + operator

/*
what flavor of value does the operator take
what flavor of value does the operator return
*/

/*
+ takes two r-values and returns an r-value
*/

k = (2 + 3);
++(i + j);   // no
(i + j) = k; // no

int i = 2;
int j = ++i;
cout << i;   // 3
cout << j;   // 3

int i = 2;
int j = i++; // postfix is inherently slower than prefix
cout << i;   // 3
cout << j;   // 2

/*
++ (prefix) takes an l-value and returns l-value
*/

++i;
++2; // no

(++i) = 3; // ok, i would become 3 (C++ only, not true in C, not true in Java)

/*
++ (postfix) takes an l-value and returns r-value
*/

(i++) = 3; // no

int i = 2;
++++i;     // yes!!!
i++++;     // no!!!
++i++;     // no, postfix must go first

int i = 2;
int j = (i += 3);
(i += 4) = 5;     // yes!!!

/*
+= takes an l-value (left) and an r-value (right) and returns an l-value
*/

/*
all flavors of = return an l-value
*/

int i = 2;
int j = 3;
int k = (i << j);
k = (2 << 3);     // yes

/*
<< (int's) takes two r-values and returns an r-value
*/

k = (i <<= j);

/*
<<= (int's) takes an l-value (left) and an r-value (right) and returns an l-value
*/

cout << "Andrew" << "Li";
cout << endl;

/*
<< (ostream's) takes an l-value (left) an r-value (right) and returns an l-value
*/

struct Complex {
	? operator +  (...) {...}
	? operator << (...) {...}
	? operator *  (...) {...}
	}

float f = 2.3;
float g = 4.5;
float h = (f << g); no

class Andrew {
	? operator ,,, (...) {...} // no
	? operator +   (...) {...}
	? operator *   (...) {...}
	? operator -   (...) {...}

? operator << (double f, double g) {...} // no

i + j * k // * has a higher precedence than +

i - j - k // i - j goes first, - is left  associative
i = j = k // j = k goes first, = is right associative

/*
rules for operator overloading

1. CAN NOT invent a new token, has to be one of the 40 or 50 that are already there

2. CAN NOT give meaning to a built-in operator that does not have a meaning

3. CAN NOT change the precedence of operators

4. CAN NOT change the associativity of operators

5. CAN NOT change the number of arguments for an operator (arity)

6. CAN change the l-value/r-value nature of the arguments and/or the return
*/

for (int i = 0; i != s; i++) {
	...
	}

{
int i = 0;
while (i != s) {
	...
	i++;}        // this is dumb, and the compiler change it to ++i
}

for (I i = 0; i != s; i++) { // the compiler CAN NOT change i++ to ++i
	...
	}

bool is_prime (int n) {
    assert(n > 0);
    if ((n == 1) || ((n % 2) == 0))
        return false;
    for (int i = 3; i < std::sqrt(n); ++i)
        if ((n % i) == 0)
            return false;
    return true;}

void test0 () {
    assert(!is_prime( 1));}

void test1 () {
    assert(!is_prime( 2));}

void test2 () {
    assert( is_prime( 3));}

void test3 () {
    assert(!is_prime( 4));}

void test4 () {
    assert( is_prime( 5));}

void test5 () {
    assert( is_prime( 7));}

void test6 () {
    assert( is_prime( 9));}

void test7 () {
    assert(!is_prime(27));}

void test8 () {
    assert( is_prime(29));}

/*
bad code AND bad tests

1. run it as is, confirm success
2. fix the tests, NOT the code
3. run it again, confirm failure
4. fix the code
5. run it again, confirm success
*/
