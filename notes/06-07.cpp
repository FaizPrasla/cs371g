// -----------
// Mon,  7 Jun
// -----------

/*
Website
	https://www.cs.utexas.edu/users/downing/cs371g/

Cold Calling
	you're only called ONCE per rotation
	it's totally fine to not know the answer, the idea is to discuss and to learn

Lecture
	1-3 pm

Office Hours
	Glenn
		Austin
		TTh 3-4 pm

	Amogh ('22)
		Austin
		MW 3-4 pm

Canvas
	personal questions
	check your grades regularly!

Discord
	chat
	please be generous and kind

GitLab
	notes
	examples
	exercises

Piazza
	class questions
	ask and answer questions
	please be proactive

Zoom
	must use UT EID credentials (@eid.utexas.edu)
	classes recorded
	published later that day
	5.6.6
*/

/*
Breakout Rooms
	7 breakout rooms

	Full Name: Room #5: <name1>, <name2>, ...
	EID:       <eid1>, <eid2>, ...

	EVERYONE turn on video
	leave the shared screen up
	stay in the breakout room
	when done ask us to visit
*/

/*
takeaways
*/

/*
reminders
    announcements
*/

// ---------
// Hello.cpp
// ---------

// https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines.html
// https://en.cppreference.com
// https://en.cppreference.com/w/cpp/io

#include <iostream> // cout, endl

int main () {
    using namespace std;
    cout << "Nothing to be done." << endl;
    return 0;}

/*
Developed in 1978 by Bjarne Stroustrup of Denmark.
C++ is procedural, object-oriented, statically typed, and not garbage collected.

C++98
C++03
C++11
C++14
C++17
C++20
C++23



% which g++-11
/usr/local/bin/g++-11



% g++-11 --version
g++-11 (Homebrew GCC 11.1.0_1) 11.1.0



% g++-11 -pedantic -std=c++17 -O3 -Wall Hello.cpp -o Hello



% ./Hello
Nothing to be done.
*/

/*
Collatz Conjecture
	it's about 100 years

given a pos int
if even, divide   by 2
if odd,  multiply by 3 and add 1
repeat 1
*/

5 16 8 4 2 1

/*
the cycle length of  5 is 6
the cycle length of 10 is 7
*/

// --------------
// Assertions.cpp
// --------------

// https://en.cppreference.com/w/cpp/error/assert

#include <cassert>  // assert
#include <iostream> // cout, endl

using namespace std;

unsigned cycle_length (unsigned n) {
    assert(n > 0);  // precondition
    unsigned c = 1; // the fix
    while (n > 1) {
        if ((n % 2) == 0)
            n = (n / 2);
        else
            n = (3 * n) + 1;
        ++c;}
    assert(c > 0); // postcondition
    return c;}

void test () {
    assert(cycle_length( 1) == 1);
    assert(cycle_length( 5) == 6);
    assert(cycle_length(10) == 7);}

int main () {
    cout << "Assertions.cpp" << endl;
    test();
    cout << "Done." << endl;
    return 0;}

/*
% g++-11 -pedantic -std=c++17 -O3 -Wall Assertions.cpp -o Assertions
% ./Assertions
Assertions.cpp
Assertion failed: (c > 0), function cycle_length, file Assertions.cpp, line 21.



Turn OFF assertions at compile time with -DNDEBUG
% g++-11 -pedantic -std=c++17 -O3 -DNDEBUG -Wall Assertions.cpp -o Assertions
% ./Assertions
Assertions.cpp
Done.
*/

/*
help you debug
help you reason about the code
they're good for preconditions, postcondtions, loop invariants
they're bad  for testing,     what will be good, a unit test framework, Google Test
they're bad  for user errors, what will be good, exceptions
*/

// --------------
// UnitTests1.cpp
// --------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/docs/reference/assertions.md

#include <cassert>  // assert
#include <iostream> // cout, endl

#include "gtest/gtest.h"

int cycle_length (int n) {
    assert(n > 0);
    int c = 0;
    while (n > 1) {
        if ((n % 2) == 0)
            n = (n / 2);
        else
            n = (3 * n) + 1;
        ++c;}
    assert(c > 0);
    return c;}

TEST(UnitTestsFixture, test1) {
    ASSERT_EQ(cycle_length( 1), 1);}

TEST(UnitTestsFixture, test2) {
    ASSERT_EQ(cycle_length( 5), 6);}

TEST(UnitTestsFixture, test3) {
    ASSERT_EQ(cycle_length(10), 7);}

/*
% g++-11 -pedantic -std=c++17 -O3 -Wall UnitTests1.cpp -o UnitTests1 -lgtest -lgtest_main



% ./UnitTests1
Running main() from gtest_main.cc
[==========] Running 3 tests from 1 test case.
[----------] Global test environment set-up.
[----------] 3 tests from UnitTestsFixture
[ RUN      ] UnitTestsFixture.test1
Assertion failed: (c > 0), function cycle_length, file UnitTests1.cpp, line 23.
*/

// --------------
// UnitTests2.cpp
// --------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/docs/reference/assertions.md

#include <cassert>  // assert
#include <iostream> // cout, endl

#include "gtest/gtest.h"

int cycle_length (int n) {
    assert(n > 0);
    int c = 1;
    while (n > 1) {
        if ((n % 2) == 0)
            n = (n / 2);
        else
            n = (3 * n) + 1;
        ++c;}
    assert(c > 0);
    return c;}

TEST(UnitTestsFixture, test1) {
    ASSERT_EQ(cycle_length( 1), 1);}

TEST(UnitTestsFixture, test2) {
    ASSERT_EQ(cycle_length( 5), 5);}

TEST(UnitTestsFixture, test3) {
    ASSERT_EQ(cycle_length(10), 7);}

/*
% ./UnitTests2
Running main() from /Users/downing/src/googletest-master/googletest/src/gtest_main.cc
[==========] Running 3 tests from 1 test case.
[----------] Global test environment set-up.
[----------] 3 tests from UnitTestsFixture
[ RUN      ] UnitTestsFixture.test1
[       OK ] UnitTestsFixture.test1 (0 ms)
[ RUN      ] UnitTestsFixture.test2
UnitTests2.cpp:29: Failure
Expected equality of these values:
  cycle_length( 5)
    Which is: 6
  5
[  FAILED  ] UnitTestsFixture.test2 (0 ms)
[ RUN      ] UnitTestsFixture.test3
[       OK ] UnitTestsFixture.test3 (0 ms)
[----------] 3 tests from UnitTestsFixture (0 ms total)

[----------] Global test environment tear-down
[==========] 3 tests from 1 test case ran. (0 ms total)
[  PASSED  ] 2 tests.
[  FAILED  ] 1 test, listed below:
[  FAILED  ] UnitTestsFixture.test2

 1 FAILED TEST
*/

// --------------
// UnitTests3.cpp
// --------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/docs/reference/assertions.md

#include <cassert>  // assert
#include <iostream> // cout, endl

#include "gtest/gtest.h"

int cycle_length (int n) {
    assert(n > 0);
    int c = 1;
    while (n > 1) {
        if ((n % 2) == 0)
            n = (n / 2);
        else
            n = (3 * n) + 1;
        ++c;}
    assert(c > 0);
    return c;}

TEST(UnitTestsFixture, test1) {
    ASSERT_EQ(cycle_length( 1), 1);}

TEST(UnitTestsFixture, test2) {
    ASSERT_EQ(cycle_length( 5), 6);}

TEST(UnitTestsFixture, test3) {
    ASSERT_EQ(cycle_length(10), 7);}

/*
% ./UnitTests3
Running main() from gtest_main.cc
[==========] Running 3 tests from 1 test case.
[----------] Global test environment set-up.
[----------] 3 tests from UnitTestsFixture
[ RUN      ] UnitTestsFixture.test1
[       OK ] UnitTestsFixture.test1 (0 ms)
[ RUN      ] UnitTestsFixture.test2
[       OK ] UnitTestsFixture.test2 (0 ms)
[ RUN      ] UnitTestsFixture.test3
[       OK ] UnitTestsFixture.test3 (0 ms)
[----------] 3 tests from UnitTestsFixture (0 ms total)

[----------] Global test environment tear-down
[==========] 3 tests from 1 test case ran. (0 ms total)
[  PASSED  ] 3 tests.
*/

// -------------
// Coverage1.cpp
// -------------

// https://gcc.gnu.org/onlinedocs/gcc/Gcov.html

#include <cassert>  // assert
#include <iostream> // cout, endl

#include "gtest/gtest.h"

int cycle_length (int n) {
    assert(n > 0);
    int c = 1;
    while (n > 1) {
        if ((n % 2) == 0)
            n = (n / 2);
        else
            n = (3 * n) + 1;
        ++c;}
    assert(c > 0);
    return c;}

TEST(CoverageFixture, test) {
    ASSERT_EQ(cycle_length(1), 1);}

/*
% g++-11 --coverage -pedantic -std=c++17 -O3 -Wall Coverage1.cpp -o Coverage1 -lgtest -lgtest_main -pthread



% ./Coverage1
Running main() from gtest_main.cc
[==========] Running 1 test from 1 test case.
[----------] Global test environment set-up.
[----------] 1 test from CoverageFixture
[ RUN      ] CoverageFixture.test
[       OK ] CoverageFixture.test (0 ms)
[----------] 1 test from CoverageFixture (0 ms total)

[----------] Global test environment tear-down
[==========] 1 test from 1 test case ran. (0 ms total)
[  PASSED  ] 1 test.



% ls -al Coverage1.*
-rw-r--r--@ 1 downing  staff     1314 Jun  7 11:38 Coverage1.cpp
-rw-r--r--@ 1 downing  staff     2359 Jun  7 11:39 Coverage1.cpp.gcov
-rw-r--r--@ 1 downing  staff     1232 Jun  7 11:40 Coverage1.gcda
-rw-r--r--@ 1 downing  staff    22760 Jun  7 11:39 Coverage1.gcno



% gcov-11 Coverage1.cpp | grep -B 2 "cpp.gcov"
File 'Coverage1.cpp'
Lines executed:60.00% of 10
Creating 'Coverage1.cpp.gcov'
*/

// -------------
// Coverage2.cpp
// -------------

// https://gcc.gnu.org/onlinedocs/gcc/Gcov.html

#include <cassert>  // assert
#include <iostream> // cout, endl

#include "gtest/gtest.h"

int cycle_length (int n) {
    assert(n > 0);
    int c = 1;
    while (n > 1) {
        if ((n % 2) == 0)
            n = (n / 2);
        else
            n = (3 * n) + 1;
        ++c;}
    assert(c > 0);
    return c;}

TEST(CoverageFixture, test) {
    ASSERT_EQ(cycle_length(2), 2);}

/*
% ./Coverage2
Running main() from gtest_main.cc
[==========] Running 1 test from 1 test case.
[----------] Global test environment set-up.
[----------] 1 test from CoverageFixture
[ RUN      ] CoverageFixture.test
[       OK ] CoverageFixture.test (0 ms)
[----------] 1 test from CoverageFixture (0 ms total)

[----------] Global test environment tear-down
[==========] 1 test from 1 test case ran. (0 ms total)
[  PASSED  ] 1 test.



% gcov-11 Coverage2.cpp | grep -B 2 "cpp.gcov"
File 'Coverage2.cpp'
Lines executed:90.00% of 12
Creating 'Coverage2.cpp.gcov'
*/

// -------------
// Coverage3.cpp
// -------------

// https://gcc.gnu.org/onlinedocs/gcc/Gcov.html

#include <cassert>  // assert
#include <iostream> // cout, endl

#include "gtest/gtest.h"

int cycle_length (int n) {
    assert(n > 0);
    int c = 1;
    while (n > 1) {
        if ((n % 2) == 0)
            n = (n / 2);
        else
            n = (3 * n) + 1;
        ++c;}
    assert(c > 0);
    return c;}

TEST(CoverageFixture, test) {
    ASSERT_EQ(cycle_length(3), 8);}

/*
% ./Coverage3
Running main() from gtest_main.cc
[==========] Running 1 test from 1 test case.
[----------] Global test environment set-up.
[----------] 1 test from CoverageFixture
[ RUN      ] CoverageFixture.test
[       OK ] CoverageFixture.test (0 ms)
[----------] 1 test from CoverageFixture (0 ms total)

[----------] Global test environment tear-down
[==========] 1 test from 1 test case ran. (0 ms total)
[  PASSED  ] 1 test.



% gcov-11 -b Coverage3.cpp | grep -B 2 "cpp.gcov"
File 'Coverage3.cpp'
Lines executed:100.00% of 12
Creating 'Coverage3.cpp.gcov'
*/

// input

1 10
100 200
201 210
900 1000

// output

1 10 20
100 200 125
201 210 89
900 1000 174

/*
read eval print loop (REPL)

skeleton code for this project
	cs371g-collatz GitLab repo
*/

/*
the kernel of the code
	Collatz.hpp
	Collatz.cpp

a run harness
	RunCollatz.cpp

a test harness
	TestCollatz.cpp
*/

// ---------
// Collatz.hpp
// ---------

#ifndef Collatz_h
#define Collatz_h

// ----------------
// max_cycle_length
// ----------------

/**
 * @param two positive ints
 * @return one positive int
 */
unsigned max_cycle_length (unsigned, unsigned);

#endif // Collatz_h



// Java

class T {
	void foo (int i) {
		++i;}

	void bar () {
		int i = 2;
		foo(i);
		System.out.println(i);




// --------------
// RunCollatz.c++
// --------------

// --------
// includes
// --------

#include <iostream> // cin, cout, endl
#include <sstream>  // istringstream
#include <string>   // getline, string

#include "Collatz.hpp"

using namespace std;

// ----
// main
// ----

// REPL

// declaration of getline

istream& getline(istream& in, string& s);

/*
there's an implicit conversion between istream and bool
*/

int main () {
    string s;
    while (getline(cin, s)) {
        // ----
        // read
        // ----

        istringstream iss(s); "1 10"
        unsigned i, j;
        iss >> i >> j;

        // ----
        // eval
        // ----

        unsigned v = max_cycle_length(i, j);

        // -----
        // print
        // -----
        cout << i << " " << j << " " << v << endl;}

    return 0;}

foo x;
foo y = 1; // initializing y
foo y(1);  // same thing
foo y{1};  // same thing
y = 1;     // assigning    y

int i = 2;
cout << i; // outputs i

int k;
cin >> k;  // input to k

// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

/*
write many more tests of those other functions
*/

// ----------------
// max_cycle_length
// ----------------

TEST(CollatzFixture, max_cycle_length_0) {
    ASSERT_EQ(max_cycle_length(1, 10), 11u);}

TEST(CollatzFixture, max_cycle_length_1) {
    ASSERT_EQ(max_cycle_length(100, 200), 300u);}

TEST(CollatzFixture, max_cycle_length_2) {
    ASSERT_EQ(max_cycle_length(201, 210), 411u);}

TEST(CollatzFixture, max_cycle_length_3) {
    ASSERT_EQ(max_cycle_length(900, 1000), 1900u);}


// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert> // assert

#include "Collatz.hpp"

using namespace std;

// ----------------
// max_cycle_length
// ----------------

/*
you are going to write many other funcitons
*/

unsigned max_cycle_length (unsigned i, unsigned j) {
    assert(i > 0);
    assert(j > 0);

    // <your code>
    unsigned v = i + j;                              // replace!

    assert(v > 0);
    return v;}

/*
1. clone the code repo
2. run the code as is, confirm success
3. fix the tests
4. run it again, confirm failure
5. write more tests
6. fix the code, implement the solution
7. run it again, confirm success

8. now we go to HackerRank
	merge Collatz.cpp and RunCollatz.cpp into a third HackerRankCollatz.cpp, remove the #include line
9. then you see how many of the 3 HackerRank tests you pass
*/

/*
the challenge is for your code to be fast enough
*/

/*
1. always write the stupidest solution
2. write more tests
3. make it fast
*/
