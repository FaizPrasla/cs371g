// -----------
// Mon, 14 Jun
// -----------

/*
Website
	https://www.cs.utexas.edu/users/downing/cs371g/

Cold Calling
	you're only called ONCE per rotation
	it's totally fine to not know the answer, the idea is to discuss and to learn

Lecture
	1-3 pm

Office Hours
	Glenn
		Austin
		TTh 3-4 pm

	Amogh ('22)
		Austin
		MW 3-4 pm

Canvas
	personal questions
	check your grades regularly!

Discord
	chat
	please be generous and kind

GitLab
	notes
	examples
	exercises

Piazza
	class questions
	ask and answer questions
	please be proactive

Zoom
	must use UT EID credentials (@eid.utexas.edu)
	classes recorded
	published later that day
	5.6.6
*/

/*
Breakout Rooms
	Full Name: Room #5: <name1>, <name2>, ...
	EID:       <eid1>, <eid2>, ...

	EVERYONE turn on video
	leave the shared screen up
	stay in the breakout room
	when done ask us to visit
	after the visit from me or Amogh, then submit
*/

/*
takeaways
	assertions are good for preconditions, postconditions, and loop invariants
	they're also good as computational comments
	they're not good for testing
	they're not good for user errors
	for standalone increment prefer pre-increment
	pass by reference can make certain kinds of errors compile time errors
*/

/*
reminders
	Zoom 5.6.7
    announcements
	Academic Integrity Quiz
	blogs and papers on Canvas
	Collatz Poll
	only invite Amogh to your PRIVATE code repo
*/

int i = 2;
int j = i; // make a copy
++j;
cout << i;

/*
two tokens:   *, &
two contexts: modifying a variable, modifying a type
*/

// a pointer holds on to the address of something

int i = 2;
int* p;     // pointer to an int, undefined value
p = i;      // no
p = &i;     // take the address of i, must be an l-value
// ++p;     // yes!
++*p;       // modify the target of the pointer p
cout << i;  // 3
cout << *p; // 3, * can take an r-value

int j = 4;
p = &j;     // reassign p

float f = 2.3;
p = &f;        // no

// a reference is an alias to something

int i = 2;
int& r;        // no, a reference must be initialized
int& r = &i;   // no
int& r = i;    // r and i become indistinguishable
++i;
cout << i;     // 3
cout << r;     // 3
++r;
cout << i;     // 4
cout << r;     // 4

int j = 5;
r = j;         // assigning j to i/r
cout << i;     // 5
cout << r;     // 5

int k = 6;
i = k;
cout << i;     // 6
cout << r;     // 6

// -------------
// Variables.cpp
// -------------

// https://en.cppreference.com/w/cpp/language/pointer
// https://en.cppreference.com/w/cpp/language/reference

#include <cassert>   // assert
#include <iostream>  // cout, endl

using namespace std;

void test1 () {
    int i = 2;
    int v = i;
    ++v;
    assert(i  == 2);
    assert(v  == 3);
    assert(&i != &v);}

void test2 () {
    int  j = 2;
    int* p;
//  p = j;             // error: invalid conversion from 'int' to 'int*' [-fpermissive]
    p = &j;
    ++*p;
    assert(j  == 3);
    assert(*p == 3);
    assert(p  == &j);}

void test3 () {
    int  k = 2;
//  int& r;            //  error: 'r' declared as reference but not initialized
//  int& r = &k;       // error: invalid conversion from 'int*' to 'int' [-fpermissive]
    int& r = k;
    ++r;
    assert(k  == 3);
    assert(r  == 3);
    assert(&r == &k);}

void test4 () {
    int   i = 2;
    int*  p = &i;
    int*& r = p;
    ++*r;
    assert(i  == 3);
    assert(*p == 3);
    assert(*r == 3);
    assert(&r == &p);}

int main () {
    cout << "Variables.cpp" << endl;
    test1();
    test2();
    test3();
    test4();
    cout << "Done." << endl;
    return 0;}

int    i   = 2;
int*   p   = &i;
int**  pp  = &p;
int*** ppp = &pp;
...

/*
values vs pointers vs references
*/

void f (int v) {
	++v;}

int i = 2;
f(i);
cout << i; // 2




void g (int* p) {
	++p;}         // modified the pointer, not the target

int j = 3;
g(j);      // no
g(&j);
cout << j; // 3


void g (int* p) {
	++*p;}

int j = 3;
g(&j);
cout << j; // 4




void h (int& r) {
	++*r;         // no
	++r;}

int k = 4;
h(&k);     // no
h(k);
cout << k; // 5





void g (int* p) { // annotate the parameter
	++*p;}        // annotate the use

int j = 3;
g(&j);     // annotate the call
cout << j; // 4



void h (int& r) { // annotate the parameter
	++r;}         // no annotation

int k = 4;
h(k);      // no annotation
cout << k; // 5




void g (int* p) {
	++p;}         // this is a mistake

int j = 3;
g(&j);
cout << j; // not going to get a 4 anymore



void h (int& r) {
	++*r;}        // this is a mistake, NOT compile

int k = 4;
h(k);
cout << k; // 5




void g (int* p) {
	assert(p);    // check that it's not null
	++*p;}        // runtime error!!!

int j = 3;
g(&j);
cout << j; // 4
g(185);    // no
g(0);      // yes!!!



void h (int& r) {
	++r;}

int k = 4;
h(k);
cout << k; // 5
h(185);    // no, has to be an l-value
h(0);      // no



// -------------
// Arguments.cpp
// -------------

#include <cassert>  // assert
#include <iostream> // cout, endl

using namespace std;

void by_value (int v) {
    ++v;}

void test1 () {
    const int i = 2;
    by_value(i);
    assert(i == 2);}



void by_address (int* p) {
    assert(p);
    ++*p;}

void test2 () {
    int j = 3;
    by_address(&j);
    assert(j == 4);
//  const int cj = 5;
//  by_address(&cj);  // error: invalid conversion from 'const int*' to 'int*' [-fpermissive]
//  by_address(185);  // error: invalid conversion from 'int' to 'int*' [-fpermissive]
//  by_address(0);    // Assertion failed: (p), function by_address, file Arguments.cpp, line 14.
    }



void by_reference (int& r) {
    ++r;}

void test3 () {
    int k = 4;
    by_reference(k);
    assert(k == 5);
//  const int ck = 6;
//  by_reference(ck);  // error: binding reference of type 'int&' to 'const int' discards qualifiers
//  by_reference(185); // error: cannot bind non-const lvalue reference of type 'int&' to an rvalue of type 'int'
//  by_reference(0);   // error: cannot bind non-const lvalue reference of type 'int&' to an rvalue of type 'int'
    }



int main () {
    cout << "Arguments.cpp" << endl;
    test1();
    test2();
    test3();
    cout << "Done." << endl;
    return 0;}



 #define TEST0
#define TEST1
// #define TEST2
// #define TEST3

int& pre_incr (int& r) {
    return r += 1;}

int post_incr (int& r) {
    int v = r;
    r += 1;
    return v;}

#ifdef TEST0
void test0 () {
    int i = 2;
    int j = pre_incr(i); // mimicing preincrement, NOT allowed to use ++
    assert(i == 3);
    assert(j == 3);}
#endif

#ifdef TEST1
void test1 () {
    int i = 2;
    int j = pre_incr(pre_incr(i)); // returns an l-value
    assert(i == 4);
    assert(j == 4);}
#endif

#ifdef TEST2
void test2 () {
    int i = 2;
    int j = post_incr(i); // mimicing postincrement, NOT allowed to use ++
    assert(i == 3);
    assert(j == 2);}
#endif

#ifdef TEST3
void test3 () {
    int i = 2;
//  post_incr(post_incr(i)); // error: no matching function for call to 'post_incr'
    assert(i == 2);}
#endif
