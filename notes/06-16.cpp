// -----------
// Wed, 16 Jun
// -----------

/*
Website
	https://www.cs.utexas.edu/users/downing/cs371g/

Cold Calling
	you're only called ONCE per rotation
	it's totally fine to not know the answer, the idea is to discuss and to learn

Lecture
	1-3 pm

Office Hours
	Glenn
		Austin
		TTh 3-4 pm

	Amogh ('22)
		Austin
		MW 3-4 pm

Canvas
	personal questions
	check your grades regularly!

Discord
	chat
	please be generous and kind

GitLab
	notes
	examples
	exercises

Piazza
	class questions
	ask and answer questions
	please be proactive

Zoom
	must use UT EID credentials (@eid.utexas.edu)
	classes recorded
	published later that day
	5.6.6
*/

/*
Breakout Rooms
	Full Name: Room #5: <name1>, <name2>, ...
	EID:       <eid1>, <eid2>, ...

	EVERYONE turn on video
	leave the shared screen up
	stay in the breakout room
	when done ask us to visit
	after the visit from me or Amogh, then submit
*/

/*
takeaways
	assertions are good for preconditions, postconditions, and loop invariants
	they're also good as computational comments
	they're not good for testing
	they're not good for user errors
	for standalone increment prefer pre-increment
	pass by reference can make certain kinds of errors compile time errors
*/

/*
reminders
	Zoom 5.6.7
    announcements
	blogs and papers on Canvas
	quizzes
*/

