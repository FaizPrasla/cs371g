// -----------
// Fri,  4 Jun
// -----------

/*
Website
	https://www.cs.utexas.edu/users/downing/cs371g/

Cold Calling
	you're only called ONCE per rotation
	it's totally fine to not know the answer, the idea is to discuss and to learn

Lecture
	1-3 pm

Office Hours
	Glenn
		Austin
		TTh 3-4 pm

	Amogh ('22)
		Austin
		MW 3-4 pm

Canvas
	personal questions
	check your grades regularly!

Discord
	chat
	please be generous and kind

GitLab
	notes
	examples
	exercises

Piazza
	class questions
	ask and answer questions
	please be proactive

Zoom
	must use UT EID credentials (@eid.utexas.edu)
	classes recorded
	published later that day
	5.6.6
*/

/*
Breakout Rooms
	7 breakout rooms

	Full Name: Room #5: <name1>, <name2>, ...
	EID:       <eid1>, <eid2>, ...

	EVERYONE turn on video
	leave the shared screen up
	stay in the breakout room
	when done ask us to visit
*/

/*
takeaways
*/

/*
reminders
    announcements
*/

/*
Discord
https://discord.gg/ZU8jKz6J
*/

/*
getting to know somebody
name
contact info

How was your name chosen?
Were you named after someone?
Does it have any cultural or historical meaning?
Is there a story or experience related to your name you would like to share?

When are they graduating? then what?
Why are they taking the class? why now?
What are their expectations of the class?

google "cs371g summer 2020 final entry"
*/

/*
competitive programming

International Collegiate Programming Contest (ICPC)

local contests
	at UT biweekly in the fall and spring
	regular and beginners

platforms
	HackerRank
	Codeforces
	Kattis
	Online Judge

languages
	C
	C++
	Python
	Kotlin

I get involved 2014 with Dr. Mitra

CS 104c: Competitive Programming
https://www.cs.utexas.edu/users/downing/cs104c/
	fall and spring

regional contests
	135 regions in the world
	11 regions in north america
	our region
		at Baylor in Waco
		3 states (TX, OK, LA)
		25 schools
		65 teams (teams of 3)
		we send 4 teams, 12 students
	if you win you go the world finals

2014; 2nd, Rice, 1st, Morocco
2015; 2nd, Rice, 1st, Thailand, all 4 teams were in the top 7 of the 65
2016; 1st, 2nd, 3rd, 4th, South Dakota!!!
2017; 1st, Beijing
2018; 1st, Portugal
2019; 1st, Moscow (Oct 2022)

UT Programming Contest (UTPC)
https://www.cs.utexas.edu/users/utpc/

https://discord.gg/NGRMXNHe

you have or you will take the algorithms class
	pure math class, no coding
	competitive programming is the realization of those algorithms
*/

/*
three ways to use the same tool chain
1. SSH into the CS machines
2. install those tools on your machine
3. install Docker and use a Docker image that I built
*/

/*
% which docker
/usr/local/bin/docker



% docker --version
Docker version 20.10.6, build 370c289



% docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE



% cat Dockerfile
FROM gcc

RUN apt-get update

RUN apt-get -y install astyle
RUN apt-get -y install cppcheck
RUN apt-get -y install cmake
RUN apt-get -y install dos2unix
RUN apt-get -y install doxygen
RUN apt-get -y install graphviz
RUN apt-get -y install valgrind
RUN apt-get -y install vim
RUN apt-get -y install libboost-dev
RUN apt-get -y install libgmp-dev
RUN apt-get -y install libgtest-dev

RUN git clone https://github.com/DOMjudge/checktestdata checktestdata && \
    cd checktestdata                                                  && \
    git checkout release                                              && \
    ./bootstrap                                                       && \
    make                                                              && \
    cp checktestdata /usr/bin                                         && \
    cd -

RUN cd /usr/src/gtest                                                 && \
    cmake CMakeLists.txt                                              && \
    make                                                              && \
    cp lib/*.a /usr/lib                                               && \
    cd -

CMD bash



% docker build -t gpdowning/gcc .
...



% docker login
...



% docker push gpdowning/gcc
...

// before this, is what I had to do to build the image
// after  this, is what you have to do to use the image

% docker pull gpdowning/gcc
...



% docker images
REPOSITORY                  TAG       IMAGE ID       CREATED         SIZE
gpdowning/gcc               latest    f0825dbb2020   7 minutes ago   1.97GB
gcc                         latest    fd300071a570   3 weeks ago     1.23GB



% pwd
/Users/downing/git/cs371p/cpp



% ls
Assertions.cpp	Dockerfile	Hello.cpp	Script.txt	makefile



% docker run -it -v /Users/downing/cs371p/git:/usr/gcc -w /usr/gcc gpdowning/gcc
root@c70871d08248:/usr/gcc# pwd
/usr/gcc



root@c70871d08248:/usr/gcc# ls
Assertions.cpp	Dockerfile	Hello.cpp	Script.txt	makefile



root@c70871d08248:/usr/gcc# which astyle
/usr/bin/astyle
root@c70871d08248:/usr/gcc# astyle --version
Artistic Style Version 3.1



root@c70871d08248:/usr/gcc# which checktestdata
/usr/bin/checktestdata
root@c70871d08248:/usr/gcc# checktestdata --version
checktestdata -- version 20210603, written by Jan Kuipers, Jaap Eldering, Tobias Werth



root@c70871d08248:/usr/gcc# which cmake
/usr/bin/cmake
root@c70871d08248:/usr/gcc# cmake --version
cmake version 3.18.4



root@c70871d08248:/usr/gcc# which cppcheck
/usr/bin/cppcheck
root@c70871d08248:/usr/gcc# cppcheck --version
Cppcheck 2.3



root@c70871d08248:/usr/gcc# which doxygen
/usr/bin/doxygen
root@c70871d08248:/usr/gcc# doxygen --version
1.9.1



root@c70871d08248:/usr/gcc# which g++
/usr/local/bin/g++
root@c70871d08248:/usr/gcc# g++ --version
g++ (GCC) 11.1.0



root@c70871d08248:/usr/gcc# which gcov
/usr/local/bin/gcov
root@c70871d08248:/usr/gcc# gcov --version
gcov (GCC) 11.1.0



root@c70871d08248:/usr/gcc# which git
/usr/bin/git
root@c70871d08248:/usr/gcc# git --version
git version 2.30.2



root@c70871d08248:/usr/gcc# which make
/usr/bin/make
root@c70871d08248:/usr/gcc# make --version
GNU Make 4.3



root@c70871d08248:/usr/gcc# which valgrind
/usr/bin/valgrind
root@c70871d08248:/usr/gcc# valgrind --version
valgrind-3.16.1



root@c70871d08248:/usr/gcc# which vim
/usr/bin/vim
root@c70871d08248:/usr/gcc# vim --version
VIM - Vi IMproved 8.2 (2019 Dec 12, compiled Mar 02 2021 02:58:09)



root@c70871d08248:/usr/gcc#  grep "#define BOOST_LIB_VERSION " /usr/include/boost/version.hpp
#define BOOST_LIB_VERSION "1_74"



root@c70871d08248:/usr/gcc# grep "set(GOOGLETEST_VERSION" /usr/src/gtest/CMakeLists.txt
set(GOOGLETEST_VERSION 1.10.0)



root@c70871d08248:/usr/gcc# ls -al /usr/lib/libgtest*.a
-rw-r--r-- 1 root root 2448250 Jun  3 23:34 /usr/lib/libgtest.a
-rw-r--r-- 1 root root    4012 Jun  3 23:34 /usr/lib/libgtest_main.a
*/
