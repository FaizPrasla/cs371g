// ---------
// Hello.cpp
// ---------

// https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines.html
// https://en.cppreference.com
// https://en.cppreference.com/w/cpp/io

#include <iostream> // cout, endl

int main () {
    using namespace std;
    cout << "Nothing to be done." << endl;
    return 0;}

/*
Developed in 1978 by Bjarne Stroustrup of Denmark.
C++ is procedural, object-oriented, statically typed, and not garbage collected.

C++98
C++03
C++11
C++14
C++17
C++20
C++23



% which g++-11
/usr/local/bin/g++-11



% g++-11 --version
g++-11 (Homebrew GCC 11.1.0_1) 11.1.0



% g++-11 -pedantic -std=c++17 -O3 -Wall Hello.cpp -o Hello



% ./Hello
Nothing to be done.
*/
